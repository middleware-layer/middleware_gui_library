#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: middleware_gui_library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

coloredMessage(BoldYellow "Loading Production Source CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

if(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME})
else()
endif()

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY})

add_subdirectory(TouchGFX)

#############################################
#   A.1. Set Options                        #
#############################################

set(SOURCES ${TouchGFX_Sources}
            #${PROJECT_PRODUCTION_SOURCE_FOLDER}/
            )

set(HEADERS ${TouchGFX_Headers})

set(PRODUCTION_SOURCES ${TouchGFX_Sources}
                       #${PROJECT_PRODUCTION_SOURCE_FOLDER}/
                       PARENT_SCOPE)

set(PRODUCTION_HEADERS ${TouchGFX_Headers}
                       PARENT_SCOPE)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/
                    ${CMAKE_CURRENT_SOURCE_DIR}/../)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

if(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME})
else()
    add_library(${APP_NAME} STATIC
                ${SOURCES})
endif()
