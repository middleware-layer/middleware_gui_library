#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_GUI_Library                                               #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading TouchGFX Include CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_INC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/TouchGFX/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(TouchGFX_Sub_Headers  #${TouchGFX_Containers_Headers}
                          PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################
