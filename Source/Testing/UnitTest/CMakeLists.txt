#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: middleware_gui_library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

coloredMessage(BoldYellow "Loading Unit Test Source CMakeLists" STATUS)

coloredMessage(BoldRed "Unit Tests for Middleware GUI are currently unsupported due to the lack of a 32 bits touchgfx static library" WARNING)

set(UNIT_TEST_APP_NAME ${PROJECT_FULLNAME}-Test)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

check_gtest_gmock_dependency()

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

add_subdirectory(${PROJECT_PRODUCTION_SOURCE_FOLDER} ../../Production)

#############################################
#   A.1. Set Options                        #
#############################################

set(SOURCES #${PRODUCTION_SOURCES}
            ${PROJECT_UNIT_TESTING_SOURCE_FOLDER}/AllTests.cpp)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#link_directories(${PROJECT_PRODUCTION_SOURCE_FOLDER}/TouchGFX/Libs/linux)

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

add_executable(${UNIT_TEST_APP_NAME}
               ${SOURCES})

link_gtest_gmock_libraries()

target_link_libraries(${UNIT_TEST_APP_NAME}
                      pthread)
                      #touchgfx)

add_test(NAME ${UNIT_TEST_APP_NAME}
         COMMAND ${UNIT_TEST_APP_NAME})
