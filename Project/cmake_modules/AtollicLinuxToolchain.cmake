#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: middleware_gui_library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

SET(CMAKE_C_COMPILER                    gcc)
SET(CMAKE_CXX_COMPILER                  g++)

set(COMPILER_FLAGS_GCC_VERSION  "-std=gnu11")
set(COMPILER_FLAGS_G++_VERSION  "-std=gnu++14")

set(COMPILER_FLAGS_TARGET       "-m32")

set(COMPILER_FLAGS_GENERAL      "")
set(COMPILER_FLAGS_G++_GENERAL  "")

if(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME})
    set(COMPILER_FLAGS_OPT_DEBUGGING        "-O2")
else()
    set(COMPILER_FLAGS_OPT_DEBUGGING        "-O2 -g3")
endif()

set(COMPILER_FLAGS_WARNING      "")

set(COMPILER_PROJECT_CONFIGS        "${PROJECT_CONFIG_DEFINES} ${EXTERNAL_DEFINES}")

set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   ${COMPILER_FLAGS_TARGET} ${COMPILER_FLAGS_GCC_VERSION} ${COMPILER_FLAGS_OPT_DEBUGGING} ${COMPILER_FLAGS_GENERAL} ${COMPILER_FLAGS_WARNING} ${COMPILER_PROJECT_CONFIGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMPILER_FLAGS_TARGET} ${COMPILER_FLAGS_G++_VERSION} ${COMPILER_FLAGS_OPT_DEBUGGING} ${COMPILER_FLAGS_GENERAL} ${COMPILER_FLAGS_WARNING} ${COMPILER_PROJECT_CONFIGS} ${COMPILER_FLAGS_G++_GENERAL}")

coloredMessage(BoldGreen "GCC Compiler Options:" STATUS)
coloredMessage(BoldWhite "${CMAKE_C_FLAGS}" STATUS)
coloredMessage(BoldGree  "G++ Compiler Options:" STATUS)
coloredMessage(BoldWhite "${CMAKE_CXX_FLAGS}" STATUS)
