#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: middleware_gui_library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

if(NOT DEFINED GTEST_PROJ_NAME)
    set(GTEST_PROJ_NAME                 "googletest")
endif()

set(GTEST_GIT_REPOSITORY            "https://github.com/google/googletest.git")
set(GTEST_GIT_TAG                   "master")

set(GTEST_PROJECT_ROOT_FOLDER       "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${GTEST_PROJ_NAME}-src/")

if(NOT DEFINED GTEST_PROJECT_LIB_FOLDER)
    set(GTEST_PROJECT_LIB_FOLDER        "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${GTEST_PROJ_NAME}-src/lib")
endif()

if(NOT DEFINED GTEST_PROJECT_INCLUDE_FOLDER)
    set(GTEST_PROJECT_INCLUDE_FOLDER    "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${GTEST_PROJ_NAME}-src/googletest/include")
endif()

if(NOT DEFINED GMOCK_PROJECT_INCLUDE_FOLDER)
    set(GMOCK_PROJECT_INCLUDE_FOLDER    "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${GTEST_PROJ_NAME}-src/googlemock/include")
endif()

include(DownloadProject)

function(check_gtest_gmock_dependency)
    coloredMessage(BoldMagenta "Well, it seems I need Google Test and Google Mock Framework. Let's see if I can find it!" STATUS)

    find_library(GTEST_LIB 
                 NAMES      gtest
                 HINTS      ${GTEST_PROJECT_LIB_FOLDER}
                 PATHS      ${GTEST_PROJECT_LIB_FOLDER})

    find_library(GMOCK_LIB 
                 NAMES      gmock
                 HINTS      ${GTEST_PROJECT_LIB_FOLDER}
                 PATHS      ${GTEST_PROJECT_LIB_FOLDER})

    if(GTEST_LIB AND GMOCK_LIB)
        coloredMessage(BoldMagenta "We have the libraries, let me just include the directory and link them to you (:" STATUS)
    else()
        coloredMessage(BoldMagenta "I couldn't find them, let me download and compile it for you (:" STATUS)
    
        #First download the project
        download_project(PROJ                ${GTEST_PROJ_NAME}
                         GIT_REPOSITORY      ${GTEST_GIT_REPOSITORY}
                         GIT_TAG             ${GTEST_GIT_TAG}
                         ${UPDATE_DISCONNECTED_IF_AVAILABLE}
                         QUIET)
    
        set(cmd_cmake   "cmake")
        set(arg         "-DCMAKE_CXX_FLAGS=-m32")
        set(cmd_make    "make")

        #Second, compile it
        execute_process(COMMAND             ${cmd_cmake} ${arg}
                        WORKING_DIRECTORY   ${GTEST_PROJECT_ROOT_FOLDER}
                        OUTPUT_QUIET)

        #Finally, run make
        execute_process(COMMAND             ${cmd_make}
                        WORKING_DIRECTORY   ${GTEST_PROJECT_ROOT_FOLDER}
                        OUTPUT_QUIET)
    endif()

    link_directories(${GTEST_PROJECT_LIB_FOLDER})

    include_directories(${GTEST_PROJECT_INCLUDE_FOLDER})
    include_directories(${GMOCK_PROJECT_INCLUDE_FOLDER})
endfunction()

function(link_gtest_gmock_libraries)
    target_link_libraries(${UNIT_TEST_APP_NAME}
        libgtest.a
        libgmock.a
    )
endfunction()
