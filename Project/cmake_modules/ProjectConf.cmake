#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: middleware_gui_library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

###############################
### GENERAL CONFIGURATIONS  ###
###############################

option(USE_COTIRE                   "Use the COmpilation TIme REducer."                     ON)
option(USE_BOOST                    "Use the Boost Library."                                OFF)
option(STATICLIB_SWITCH             "Compile a statically linked version of the library."   OFF)

#########################################
### GENERAL MIDDLEWARE CONFIGURATIONS ###
#########################################

set(DEFAULT_MIDDLEWARE                  "MIDDLEWARE_USE_STM32F7")
